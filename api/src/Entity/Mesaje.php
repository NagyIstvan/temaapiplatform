<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MesajeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MesajeRepository::class)]
#[ApiResource]
class Mesaje
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $autor;

    #[ORM\Column(type: 'string', length: 255)]
    private $titlu;

    #[ORM\Column(type: 'date')]
    private $data;

    #[ORM\Column(type: 'string', length: 255)]
    private $path;

    #[ORM\ManyToMany(targetEntity: Marturii::class)]
    private $marturii;

    public function __construct()
    {
        $this->marturii = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAutor(): ?string
    {
        return $this->autor;
    }

    public function setAutor(string $autor): self
    {
        $this->autor = $autor;

        return $this;
    }

    public function getTitlu(): ?string
    {
        return $this->titlu;
    }

    public function setTitlu(string $titlu): self
    {
        $this->titlu = $titlu;

        return $this;
    }

    public function getData(): ?\DateTimeInterface
    {
        return $this->data;
    }

    public function setData(\DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Collection<int, Marturii>
     */
    public function getMarturii(): Collection
    {
        return $this->marturii;
    }

    public function addMarturii(Marturii $marturii): self
    {
        if (!$this->marturii->contains($marturii)) {
            $this->marturii[] = $marturii;
        }

        return $this;
    }

    public function removeMarturii(Marturii $marturii): self
    {
        $this->marturii->removeElement($marturii);

        return $this;
    }
}
