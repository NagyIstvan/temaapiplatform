<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220302153736 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE marturii (id INT AUTO_INCREMENT NOT NULL, prenume VARCHAR(255) NOT NULL, continut VARCHAR(255) NOT NULL, data DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mesaje (id INT AUTO_INCREMENT NOT NULL, autor VARCHAR(255) NOT NULL, titlu VARCHAR(255) NOT NULL, data DATE NOT NULL, path VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mesaje_marturii (mesaje_id INT NOT NULL, marturii_id INT NOT NULL, INDEX IDX_5A5897328B7CBE34 (mesaje_id), INDEX IDX_5A5897328B1B9375 (marturii_id), PRIMARY KEY(mesaje_id, marturii_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mesaje_marturii ADD CONSTRAINT FK_5A5897328B7CBE34 FOREIGN KEY (mesaje_id) REFERENCES mesaje (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE mesaje_marturii ADD CONSTRAINT FK_5A5897328B1B9375 FOREIGN KEY (marturii_id) REFERENCES marturii (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mesaje_marturii DROP FOREIGN KEY FK_5A5897328B1B9375');
        $this->addSql('ALTER TABLE mesaje_marturii DROP FOREIGN KEY FK_5A5897328B7CBE34');
        $this->addSql('DROP TABLE marturii');
        $this->addSql('DROP TABLE mesaje');
        $this->addSql('DROP TABLE mesaje_marturii');
    }
}
